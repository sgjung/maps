  "devDependencies": {
    "bower": "^1.8.4",
    "browser-sync": "^2.26.3",
    "browser-sync-spa": "^1.0.3",
    "grunt": "^1.0.3",
    "grunt-contrib-connect": "^2.0.0",
    "grunt-contrib-jshint": "^2.0.0",
    "gulp": "^3.9.1",
    "gulp-load-plugins": "~0.10.0",
    "gulp-util": "~3.0.6",
    "serve-static": "^1.13.2"
  },