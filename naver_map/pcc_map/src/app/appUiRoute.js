// 'use strict';

var nuriModule = angular.module('AnyGap', ['ui.router']);

nuriModule.config(function($stateProvider, $urlRouterProvider) {
  // $locationProvider.hashPrefix('');

  $stateProvider
    .state('home', {
      url: '/',
      templateUrl: 'src/app/pcc-navermap.html',
      controller: 'MainCtrl',
      controllerAs: 'main'
    })
    .state('pcc-navermap', {
      url: '/navermap',
      templateUrl: 'src/app/pcc-navermap.html',
      controller: 'MainCtrl',
      controllerAs: 'main'
    })
    .state('pcc-d3map', {
      url: '/d3map',
      templateUrl: 'src/app/pcc-d3map.html',
      controller: 'GraphCtrl',
      controllerAs: 'graph'
    });

  $urlRouterProvider.otherwise('/');
});

nuriModule.controller('MainCtrl', function($scope) {
  var vm = this;

  var pccSitesList = [];
  var gps_sido_area = new Object();
  var map = undefined;
  var arr_contentstring = new Array();
  var contentString = undefined;
  var d3json = undefined;
  var d3features = undefined;
  var arr_marker = [];

  var tooltip = $('<div style="position:absolute;z-index:1000;padding:5px 10px;background-color:#fff;border:solid 2px #000;font-size:14px;pointer-events:none;display:none;"></div>');

  var HOME_PATH = window.HOME_PATH || '.',
    // urlPrefix = HOME_PATH +'/assets/data/region',
    urlPrefix = '/data/region',
    urlSuffix = '.json',
    regionGeoJson = [],
    loadCount = 0;

  var state = {
    clickedLocation: null
  };

  var htmlMarker1 = {
    content: '<div style="cursor:pointer;width:40px;height:40px;line-height:42px;font-size:10px;color:white;text-align:center;font-weight:bold;background:url(' + HOME_PATH + '/src/assets/img/cluster-marker-1.png);background-size:contain;"></div>',
    size: N.Size(40, 40),
    anchor: N.Point(20, 20)
  };

  var contentString = undefined;
  var infoWindowOptions = {
    content: contentString
  };

  var arr_infowindow = new Array();
  var arr_contentstring = new Array();


  // 초간단 예제 좌표들
  var geojson = undefined;
  var d3json = undefined;
  var d3features = undefined;

  var jeju = new naver.maps.LatLng(33.3590628, 126.534361),
    busan = new naver.maps.LatLng(35.1797865, 129.0750194),
    dokdo = new naver.maps.LatLngBounds(
      new naver.maps.LatLng(37.2380651, 131.8562652),
      new naver.maps.LatLng(37.2444436, 131.8786475)),
    seoul = new naver.maps.LatLngBounds(
      new naver.maps.LatLng(37.42829747263545, 126.76620435615891),
      new naver.maps.LatLng(37.7010174173061, 127.18379493229875));


  function getGeoJsonBySido() {
    for (var i = 1; i < 18; i++) {
      var keyword = i + '';

      if (keyword.length === 1) {
        keyword = '0' + keyword;
      }

      $.ajax({
        url: urlPrefix + keyword + urlSuffix,
        success: function(idx) {
          return function(geojson) {
            regionGeoJson[idx] = geojson;

            loadCount++;

            if (loadCount === 17) {
              startDataLayer();
            }
          }
        }(i - 1)
      });
    }
  }

  function createNaverMap() {
    map = new naver.maps.Map(document.getElementById('mapDiv'), {
      zoom: 2,
      mapTypeId: 'normal',
      center: new naver.maps.LatLng(36.4800580, 127.289039), //36.4203004, 128.317960
      zoomControl: true,
      zoomControlOptions: {
        position: naver.maps.Position.TOP_LEFT,
        // style: naver.maps.ZoomControlStyle.SMALL
      },
      scaleControl: true,
      draggable: true,
      scrollWheel: true
    });
  }

  function startDataLayer() {
    map.data.setStyle(function(feature) {
      var styleOptions = {
        fillColor: 'orange', //'#ff0000'
        fillOpacity: 0.0001,
        strokeColor: 'orange', //'#ff0000'
        strokeWeight: 2,
        strokeOpacity: 0.4 // 0.4
      };

      if (feature.getProperty('focus')) {
        styleOptions.fillOpacity = 0.6;
        styleOptions.fillColor = '#0f0';
        styleOptions.strokeColor = '#0f0';
        styleOptions.strokeWeight = 4;
        styleOptions.strokeOpacity = 1;
      }

      return styleOptions;
    });

    regionGeoJson.forEach(function(geojson) {
      map.data.addGeoJson(geojson);
    });

    getJsonFile('/data/sites/gps_sido_area.json', gps_sido_area)
      .then(function(data) {
        addMapListeners(data);
      });
  }

  function getGpsOfPccSites() {
    var jsonLocation = '/data/sites/pcc_sites.json';

    if (pccSitesList.length > 0)
      pccSitesList = [];

    return new Promise(function(resolve, reject) {
      $.getJSON(jsonLocation, function(data) {
        $.each(data, function(l, item) {
          pccSitesList.push(item);
          console.log('aaa :: ', item.v_site_nm);
        });
        resolve(pccSitesList);
      });
    });
  }

  function getJsonFile(filename, target) {
    var jsonLocation = filename;
    return new Promise(function(resolve, reject) {
      $.getJSON(jsonLocation, function(data) {
        if (angular.isArray(data)) {
          target = new Array();
          $.each(data, function(l, item) {
            target.push(item);
            console.log('list data ???', target);
            resolve(target);
            // console.log('filena',item.v_site_nm);
          });
        } else {
          target = data;
          resolve(data);
        }

      });
    });
  }

  function renderMarkers(data) {
    for (var i = 0; i < data.length; i++) {
      var objSite = data[i];
      console.log('renderMarkers>>', data[i].v_site_nm);

      var marker = new naver.maps.Marker({
        position: new naver.maps.LatLng((objSite.gps_lat) * 1, (objSite.gps_lng) * 1),
        map: map
      });

      contentString = [
        '<div class="iw_inner">',
        '   <h3>', objSite.v_site_nm, '</h3>',
        '   <p>', objSite.v_rest_address, '<br />',
        '   </p>',
        '</div>'
      ].join('');

      arr_contentstring.push(contentString);

      console.log("contentString>>> ", contentString);
      arr_marker.push(marker);
    }

    var markerClustering = new MarkerClustering({
      minClusterSize: 2,
      maxZoom: 10,
      map: map,
      markers: arr_marker,
      disableClickZoom: false,
      gridSize: 120,
      icons: [htmlMarker1],
      indexGenerator: [3, 100, 200, 500, 1000],
      stylingFunction: function(clusterMarker, count) {
        $(clusterMarker.getElement()).find('div:first-child').text(count);
      }
    });

    console.log('arr_marker >> ', arr_marker);
  }

  function addMarkersInfoWindow() {
    arr_marker.forEach(function(marker, index) {

      var infoWindow = new naver.maps.InfoWindow({
        content: arr_contentstring[index]
      });

      naver.maps.Event.addListener(marker, "click", function(e) {
        if (infoWindow.getMap()) {
          infoWindow.close();
        } else {
          infoWindow.open(map, marker);
        }
      });
    });
  }

  function addMapListeners(objAreaSidoData) {
    map.data.addListener('click', function(e) {
      var feature = e.feature;
      var latlng = e.coord;
      console.log('feature >>> ', feature);

      // if (feature.getProperty('focus') !== true) {
      //   feature.setProperty('focus', true);
      // }
      // else {
      //   feature.setProperty('focus', false);
      // }

      var cityNm = feature.property_area1 + '';

      console.log('feature... cityNm >>> ', cityNm);
      var sw_lat = objAreaSidoData[cityNm].gps_sw_lat;
      var sw_lng = objAreaSidoData[cityNm].gps_sw_lng;
      var ne_lat = objAreaSidoData[cityNm].gps_ne_lat;
      var ne_lng = objAreaSidoData[cityNm].gps_ne_lng;

      // map.panTo(objToMove);

      var boundsTarget = new naver.maps.LatLngBounds(
        new naver.maps.LatLng(sw_lat, sw_lng),
        new naver.maps.LatLng(ne_lat, ne_lng));

      map.panToBounds(boundsTarget);
      console.log(latlng + '');

      $('#latlng').val(latlng + '');
    });

    map.addListener('click', function(e) {
      var latlng = e.coord;
      console.log(latlng + '');
      $('#latlng').val(latlng + '');
    });


    map.data.addListener('mouseover', function(e) {
      var feature = e.feature,
        regionName = feature.getProperty('area1');

      tooltip.css({
        display: '',
        left: e.offset.x,
        top: e.offset.y
      }).text(regionName);

      map.data.overrideStyle(feature, {
        fillOpacity: 0.6,
        strokeWeight: 4,
        strokeOpacity: 1
      });
    });

    map.data.addListener('mouseout', function(e) {
      tooltip.hide().empty();
      map.data.revertStyle();
    });

    return new Promise(function(resolve, reject) {
      console.log('promise >> ');
      resolve(pccSitesList);
    });
  }

  createNaverMap();
  tooltip.appendTo(map.getPanes().floatPane);
  getGeoJsonBySido();
  getGpsOfPccSites()
    .then(function(data) {
      return new Promise(function(resolve, reject) {
        console.log('promise >> ', data);
        // data = JSON.parse(data);
        for (var i = 0; i < data.length; i++) {
          var objSite = data[i];
          console.log(objSite.v_site_nm);
        }
        resolve(pccSitesList);
      });
    })
    .then(function(data) {
      renderMarkers(pccSitesList);
      addMarkersInfoWindow();
      $("#to-jeju").on("click", function(e) {
        e.preventDefault();

        map.setCenter(jeju);
      });

      $("#to-1").on("click", function(e) {
        e.preventDefault();

        map.setZoom(1, true);
      });

      $("#to-dokdo").on("click", function(e) {
        e.preventDefault();

        map.fitBounds(dokdo);
      });

      $("#to-busan").on("click", function(e) {
        e.preventDefault();

        map.panTo(busan);
      });

      $("#to-seoul").on("click", function(e) {
        e.preventDefault();

        map.panToBounds(seoul);
      });

      $("#panBy").on("click", function(e) {
        e.preventDefault();

        map.panBy(new naver.maps.Point(10, 10));
      });
    });

});

nuriModule.controller('GraphCtrl', function($scope) {
  var vm = this;
  var width=800;
      var height=800;

      var projection = d3.geo.mercator()
        .scale(100000)
        .translate([width/2, height/2]);

      var path = d3.geo.path().projection(projection);

      var svg = d3.select("#d3map").append("svg")
        .attr("width", width)
        .attr("height",height);

      var zoom = d3.behavior.zoom()
        .translate(projection.translate())
        .scale(projection.scale())
        .scaleExtent([height, 8 * height])
        .on("zoom", zoomed);

      var g = svg.append("g").call(zoom);

      // g.append("rect")
      //   .attr("class", "background")
      //   .attr("width", width)
      //   .attr("height", height);

      ///////////////////////////////////////////////////
      var map = svg.append("g").attr("id", "map");
      var places = svg.append("g").attr("id", "places");

      var projection = d3.geo.mercator()
        .center([126.9895, 37.5651])
        .scale(100000)
        .translate([width/2, height/2]);

      var path = d3.geo.path().projection(projection);
      ///////////////////////////////////////////////////


      d3.json("seoul_municipalities_topo_simple.json", function(error, data) {
        // var features = topojson.feature(data, data.objects.seoul_municipalities_geo).features;
        console.log("info>> topojson :: ", data);
        var features = topojson.feature(data, data.objects.seoul_municipalities_geo).features;
        var geojson = data.objects.seoul_municipalities_geo;
        console.log("info>> geojson :: ", geojson);

        // g.append("g")
        //   .attr("id", "states")
        //   .selectAll("path")
        //   .data(features)
        //   .enter().append("path")
        //   .attr("d", path)
        //   .on("click", clicked);
        //
        // g.append("path")
        //   .datum(topojson.mesh(data, geojson, function(a,b){return a!== b;}))
        //   .attr("id", "state-borders")
        //   .attr("d", path);

        map.selectAll("path")
            .data(features)
          .enter().append("path")
            .attr("class", function(d) { console.log(); return "municipality c" + d.properties.code; })
            .attr("d", path);

        map.selectAll("text")
            .data(features)
          .enter().append("text")
            .attr("transform", function(d) { return "translate(" + path.centroid(d) + ")"; })
            .attr("dy", ".35em")
            .attr("class", "municipality-label")
            .text(function(d) { return d.properties.name; });
      });

      function clicked(g){
        var centroid = path.centroid(d),
            translate = projection.translate();

        projection.translate([
          translate[0] - centroid[0] + width / 2,
          translate[1] - centroid[1] + height / 2
        ]);

        zoom.translate(projection.translate());

        g.selectAll("path").transition()
            .duration(700)
            .attr("d", path);
      }

      function zoomed() {
        projection.translate(d3.event.translate).scale(d3.event.scale);
        g.selectAll("path").attr("d", path);
      }
});
